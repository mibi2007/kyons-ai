from environment import User
from d4pgagent import D4PGAgent

from acme import wrappers

def action_select_update(action):
# API call to pass selected new action to server, pass action index as input
# Modify this to match with API
    error = 0
    return error

def interact(uid):
    save_path = "./save/demo/"
    d4pg_agent = D4PGAgent()
    # d4pg_agent.load_model(save_path)

    env = User(uid)
    environment = wrappers.GymWrapper(env)
    timestep = environment.reset()
    
    #Load user's history
    env.load_user(uid)
    history = env.get_actions(uid)
    
    #Update model with last action 
    if len(history) > 0:
        timestep = environment.step(history[-1]) # Last action takes effect now with latest test result
        d4pg_agent.d4pg_agent.update()
        # d4pg_agent.save_model(save_path)
    
    #Model selects new action 
    action = d4pg_agent.d4pg_agent.select_action(timestep.observation)
    err = action_select_update(action) #Return result to app
    print('Selected action:',action)
    print('Skills:',timestep.observation, 'Actions:', environment.history)
      
if __name__ == '__main__':
    print('Input student ID:')
    uid = input()
    interact(uid)