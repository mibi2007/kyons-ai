import random
import copy
import acme
from acme import environment_loop
from acme.utils import loggers
from acme import wrappers
from acme import agents
from acme import specs
from acme.tf import networks
from acme.tf import utils as tf2_utils
from acme.agents.tf import actors as actors
from acme.agents.tf import d4pg
from acme.agents import agent
from acme.adders import reverb as adders
from acme.datasets import reverb as datasets
import sonnet as snt
import tensorflow as tf
import reverb
import dm_env
import gym
from gym import spaces
from gym.utils import seeding
import numpy as np
import math
import pickle


# epsilon greedy policy
def epsilon_greedy(q_values, epsilon):
  if epsilon < np.random.random():
    return np.argmax(q_values)
  else:
    return np.random.choice(len(q_values))
    
class ITSAgent(acme.Actor):
# Simple Q-learning agent
  def __init__(self, env_specs=None, step_size=0.1, saved_q=None):
    if saved_q!=None:
      q_table = pickle.load(open(saved_q, "rb"))
      self.Q = q_table
    else:
      state_shape = SKILL_SPACES
      # state_shape.append((NUM_QUESTIONS_PER_TEST+1))
      state_shape.append(len(ACTIONS))
      self.Q = np.zeros(state_shape)
    
    # set step size
    self.step_size = step_size
    
    # set behavior policy
    # self.policy = None
    self.behavior_policy = lambda q_values: epsilon_greedy(q_values, epsilon=0.1)
    
    # store timestep, action, next_timestep
    self.timestep = None
    self.action = None
    self.next_timestep = None

  def state_to_index(self, state):
      state = *map(int, state),
      return state
  
  def transform_state(self, state):
      # this is specifally required for the blackjack environment
      state = *map(int, state),
      return state
  
  def select_action(self, observation):
      state = self.transform_state(observation)
      return self.behavior_policy(self.Q[state])

  def observe_first(self, timestep):
      self.timestep = timestep

  def observe(self, action, next_timestep):
      self.action = action
      self.next_timestep = next_timestep  

  def update(self):
      # get variables for convenience
      state = self.timestep.observation
      _, reward, discount, next_state = self.next_timestep
      action = self.action
      
      # turn states into indices
      state = self.transform_state(state)
      next_state = self.transform_state(next_state)
      
      # Q-value update
      # print(next_state, state, action)
      td_error = reward + discount * np.max(self.Q[next_state]) - self.Q[state][action]        
      self.Q[state][action] += self.step_size * td_error
      
      # finally, set timestep to next_timestep
      self.timestep = self.next_timestep

  def get_qs(self):
    return self.Q

