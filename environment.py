import random
import copy
import acme
from acme import environment_loop
from acme.utils import loggers
from acme import wrappers
from acme import agents
from acme import specs
from acme.tf import networks
from acme.tf import utils as tf2_utils
from acme.agents.tf import actors as actors
from acme.agents.tf import d4pg
from acme.agents import agent
from acme.adders import reverb as adders
from acme.datasets import reverb as datasets
import sonnet as snt
import tensorflow as tf
import reverb
import dm_env
import gym
from gym import spaces
from gym.utils import seeding
import numpy as np
import math
import pickle


# Names of skills - English subject
SKILL_NAMES = list(range(15)) # Put in the number of skills/learning points
SKILL_INDS = range(len(SKILL_NAMES)) # For simultion demo
# Skill level: number of times an average student needs to learn to master a skill, for simulation demo
SKILL_LEVELS = [1.0]*len(SKILL_NAMES)
SKILL_SPACES = [(i+1.0) for i in SKILL_LEVELS]
# Lessons: a lesson contains certain skills, learning a lesson may grant the student its skills, for simulation demo
LESSONS = [{'id':'01','skills':[0,1,2]},{'id':'02','skills':[1,3]},{'id':'03','skills':[0,1,4]},
           {'id':'04','skills':[0,1,5]},{'id':'05','skills':[1,6]},{'id':'06','skills':[0,1,7]},
           {'id':'07','skills':[0,1,8]},{'id':'08','skills':[1,9]},{'id':'09','skills':[0,1,10]},
           {'id':'10','skills':[1,2,3,4]},{'id':'11','skills':[1,5,6,7]},{'id':'12','skills':[0,8,9,10]},
           {'id':'13','skills':[0,1,11]},{'id':'14','skills':[1,2,5,8]},{'id':'15','skills':[1,3,6,9]},
           {'id':'16','skills':[1,4,7,10]},{'id':'17','skills':[0,3,11]},{'id':'18','skills':[0,4,11]},
           {'id':'19','skills':[1,5,6,11]},{'id':'20','skills':[0,10,11]}]
# Number of questions per test
NUM_QUESTIONS_PER_TEST = 20
# Maximum number of times the student learns from the system, the course is terminated after this many lessons regardless of student's state
MAX_NUM_ACTIONS = 100
# The student takes a test every X actions
NUM_ACTIONS_PER_TEST = 5

# Put in the number of actions
ACTIONS = list(range(20))
ACTIONS.insert(0,'STOP') # Add a 'Stop' action
# ACTIONS.append('CALL_TUTOR')
penalty_weight = 1 # Modifier for penalty term in reward function

def norm_list(l):
  s = sum(l)
  new_l = [i/s for i in l]
  return new_l
  
def sigmoid(x):
  return 1 / (1 + math.exp(-x))

def question_gen(skills, skill_prob):
# For demo only
  # Randomly selects 3-4 skills which represent the knowledge needed to answer a question
  # Input: list of all skill indices
  # Returns a list of skill indices
  num_skills = random.randint(3,4)
  skill_is = np.random.choice(skills, num_skills, p=skill_prob, replace=False)
  return skill_is

def test_gen(skills,num_q):
  # For demo only
  # A test consists of num_q questions, randomly generated
  # All skills are involved
  # Returns a list of questions
  test = []
  skill_count = [0]*len(SKILL_LEVELS)
  skill_prob = norm_list(SKILL_LEVELS)
  for i in range(num_q):
    q = question_gen(skills, skill_prob)
    for skill in q:
      skill_count[skill] += 1
    test.append(q)
    skill_dist = norm_list(skill_count)
    new_skill_prob = []
    for si, v in enumerate(skill_prob):
      new_skill_prob.append(v-skill_dist[si])
    skill_prob = norm_list(skill_prob)

  return test
  
### Represents real users
class User(gym.Env):
    def __init__(self, uid):
        self.low_state = np.zeros(len(SKILL_NAMES)) # Worst state, user knows nothing
        self.high_state = np.ones(len(SKILL_NAMES)) # Best state, user's mastered the course
        self.action_space = spaces.Discrete(len(ACTIONS))
        self.observation_space = spaces.Box(self.low_state, self.high_state, dtype=np.float64)
        
        # Initialize agent's belief of masteries - state, user knows nothing at the beginning
        self.masteries = np.zeros(len(SKILL_NAMES))

        # Initialize history
        self.history = []
        
        # Latest test
        self.last_test = {}
        
        #User ID
        self.uid = uid
        
    def get_tests(self, uid):
    # Retrieve test results, call API here
    # Latest test at index 0
        tests = []
        return tests
        
    def get_actions(self, uid):
    # Retrieve selected actions, call API here
        history = []
        return history
        
    def load_user(self, uid):
        tests = self.get_tests(uid)
        if len(tests)>0:
            latest_test = tests[0]
            #Update state 
            self.masteries = test_update_masteries(latest_test)
            self.history = get_actions(uid)
    
    def forget_update_masteries(self):
    # Simulates the forget behavior of a skill after some time steps
        new_masteries = self.masteries
        num_actions = len(self.history)
        forget_rate = np.random.normal(0.05, 0.0025)
        for skill,mastery in enumerate(self.masteries):
          forget_prob = forget_rate*sigmoid(num_actions)*0.2
          loss = np.random.choice([1,0],p=[forget_prob, 1-forget_prob])
          if self.masteries[skill]>0: new_masteries[skill] -= loss
        return new_masteries
        
    def test_update_masteries(self, test):
    # Update agent's belief of user state based on latest test result
    # Modify this function to match with the test's data structure
        correct, all = {}, {}
        new_masteries = np.zeros(len(SKILL_NAMES))
        for question in test:
            answer = question['answer']
            skill = question['skill']
        if answer==1: 
        # Answers correctly means the user has mastered the corresponding skill
            new_masteries[skill] = 1
        return new_masteries
        
    def step(self, action): 
    # Performs an action on environment (user)
        done = False # check if 'STOP' action is selected
        check_mastered = True # check if user has mastered all skills
        for i,m in enumerate(self.masteries):
            if m<1: check_mastered=False

        
        reward = 0

        self.history.append(action) 
        
        if len(self.history)==MAX_NUM_ACTIONS or action=='STOP' or check_mastered==True: done=True # Terminates process if one of conditions satisfies
        if done==True:
            # If process is terminated, calculate final reward based on user's current state, more unmastered skills = more penalty
            num_unmastered_skills = len(SKILL_NAMES) - sum(self.masteries)
            reward += num_unmastered_skills*(-50)
        
        # Penalty for choosing consecutive same actions
        num_same_act = self.count_consecutive_actions(action)
        reward += (num_same_act-1)*(-50)
        
        if action in [1,len(ACTIONS)]: # action is anything but 'STOP'
            # Applies a small penalty every step to encourage agent to learn the shortest trajectory
            reward += -1*penalty_weight # penalty term
            
        # Modify this part to match with API call
        tests = self.get_tests(self.uid)     
        has_new_test=False 
        if tests!=[]:
            latest_test = tests[0]
            if latest_test['date']!=self.last_test['date']:
                has_new_test==True
                self.last_test = latest_test
        if has_new_test==True:
            # Only calculate reward when user has new test result
            score = latest_test['score']
            if len(tests)>1:
                second_last_test = tests[1] # Previous test score
                last_score = second_last_test['score']
                reward += (score - last_score -0.5)*10  # A negative reward if test score does not improve, assume that scores are integers

            if score==0: reward+=-100   # Big penalty for knowing nothing
            elif score==10: reward+=100 # Big positive reward for max score, assume max score is 10
            
            # Update agent's belief of user's state based on latest test
            self.masteries = test_update_masteries(latest_test)

        # Simulates forget behavior
        self.masteries = self.forget_update_masteries()
        
        return self._get_obs(), reward, done, {}

        
    def reset(self):
        self.masteries = np.zeros(len(SKILL_NAMES))
        self.history = []
        self.last_test = {}
        return self._get_obs()

    def _get_obs(self):
        return self.masteries

    def count_consecutive_actions(self, action):
    # Count the number of previous consecutive actions same as the current action
        count = 1
        if len(self.history)==0: return count
        else:
            temp_acts = self.history
            temp_acts.reverse()
            for act in temp_acts:
                if act==action: count+=1
                else: break
            return count

### Student simulator, used for initializing agent and demo
class SimStudent(gym.Env):
  # An observation of the environment is a list of skill mastery, each value [0,1] represents the student's learning progress of a skill
  def __init__(self, intelligence, luck=50):
    self.low_state = np.zeros(len(SKILL_NAMES))
    self.high_state = np.ones(len(SKILL_NAMES))

    self.action_space = spaces.Discrete(len(ACTIONS)) # Number of actions = Number of lessons + 1 (Stop)
    # self.observation_space = spaces.Box(0,10,shape=(len(SKILL_INDS),),dtype="float32") # State consists of mastery vector 
    # self.observation_space = spaces.MultiDiscrete(SKILL_SPACES)
    self.observation_space = spaces.Box(self.low_state, self.high_state, dtype=np.float64)

    # intelligence [0,100] determines how easy it is for the student to answer a question that requires known skills; and how fast to forget a known skill
    # luck [0,100] determines how likely the student answers a question correctly, without knowing required skills

    # learn_prob: probability the student successfully learns a skill
    self.learn_prob = np.random.normal(0.01*intelligence, 0.0005*intelligence)+0.5
    if self.learn_prob<=0: self.learn_prob = np.random.normal(0.2, 0.01)
    if self.learn_prob>1: self.learn_prob = 1
    # forget_rate: used to calculate the probability the student forgets a skill 
    self.forget_rate = np.random.normal(0.01*(100-intelligence), 0.0005*(100-intelligence)) * 0.1
    if self.forget_rate>0.2: self.forget_rate = 0.1
    if self.forget_rate<0: self.forget_rate = np.random.normal(0.05, 0.0025)
    # answer_rate1: probability the student answers a question correctly if all required skills are known
    self.answer_rate1 = np.random.normal(0.01*intelligence, 0.001*intelligence)+0.5
    if self.answer_rate1<0: self.answer_rate1 = 0
    if self.answer_rate1>1: self.answer_rate1 = 1
    # answer_rate2: flat bonus probability the student answers a question correctly if not all required skills are known
    self.answer_rate2 = np.random.normal(0.001*luck, 0.001*luck)
    if self.answer_rate2>0.5: self.answer_rate2 = 0.5
    if self.answer_rate2<0: self.answer_rate2 = 0

    # Randomize initial true skill masteries
    self.true_masteries = np.zeros(len(SKILL_INDS))
    for i,m in enumerate(self.true_masteries):
      self.true_masteries[i] = random.randint(0,SKILL_LEVELS[i])

    # # Initialize agent's belief of masteries
    # self.masteries = np.zeros(len(SKILL_INDS))
    # # self.masteries = self.true_masteries
    # # self.masteries = np.ones(len(SKILL_INDS))

    # Initial test result
    test = test_gen(SKILL_INDS,NUM_QUESTIONS_PER_TEST)
    self.masteries = self.test_update_masteries(test)
    score = self.get_test_score(test, self.true_masteries)
    self.last_score = score

    # Initialize history
    self.history = []


  def answer_question(self, question, masteries):
    raw_prob = 1
    for req in question:
      raw_prob = raw_prob*masteries[req]/SKILL_LEVELS[req]
    answer_prob = raw_prob*self.answer_rate1 + self.answer_rate2
    if answer_prob>1: answer_prob = 1
    if answer_prob<0: answer_prob = 0
    answer = np.random.choice([1,0],p=[answer_prob, 1-answer_prob])
    return answer

  def get_test_score(self, test, masteries):
    score = 0
    for question in test:
      answer = self.answer_question(question, masteries)
      if answer==1: score+=1
    return score/NUM_QUESTIONS_PER_TEST*10

  def test_update_masteries(self, test):
    correct, all = {}, {}
    new_masteries = np.zeros(len(SKILL_INDS))
    for skill in SKILL_INDS:
      correct[skill] = 0
      all[skill] = 0
    for question in test:
      answer = self.answer_question(question, self.true_masteries)
      for skill in question:
        if answer==1: correct[skill]+=1
        all[skill]+=1
    for skill in all:
      if all[skill]!=0:
        new_masteries[skill] = int(correct[skill]/all[skill]*SKILL_LEVELS[skill])
    return [int(i) for i in new_masteries]
    
  def lesson_update_masteries(self, lesson_ind):
    new_masteries = self.true_masteries
    contained_skills = LESSONS[int(lesson_ind)]['skills']
    for skill,mastery in enumerate(self.true_masteries):
      if mastery<SKILL_LEVELS[skill] and skill in contained_skills:
        gain = np.random.choice([1,0],p=[self.learn_prob, 1-self.learn_prob])
        new_masteries[skill] += gain
    return [int(i) for i in new_masteries]

  def forget_update_masteries(self):
    new_masteries = self.true_masteries
    num_actions = len(self.history)
    for skill,mastery in enumerate(self.true_masteries):
      forget_prob = self.forget_rate*sigmoid(num_actions)*0.2
      loss = np.random.choice([1,0],p=[forget_prob, 1-forget_prob])
      if self.true_masteries[skill]>0: new_masteries[skill] -= loss
    return [int(i) for i in new_masteries]

  def step(self, action):   
    done = False
    check_mastered = True
    for i,m in enumerate(self.masteries):
      if m<SKILL_LEVELS[i]: check_mastered=False

    reward = 0
    if len(self.history)>0:
      if self.history[-1]==action: reward+=-1

    self.history.append(action) 
    if action in range(len(LESSONS)):
      self.true_masteries = self.lesson_update_masteries(action)
      self.masteries = self.lesson_update_masteries(action)
      reward += -1*penalty_weight # penalty term
    if len(self.history)==MAX_NUM_ACTIONS or len(self.history)%NUM_ACTIONS_PER_TEST==0 or check_mastered==True:
      # Take a test after this action
      test = test_gen(SKILL_INDS,NUM_QUESTIONS_PER_TEST)
      self.masteries = self.test_update_masteries(test)
      score = self.get_test_score(test, self.true_masteries)
      if score==0: reward+=-100
      elif score==10: reward+=100 # max test score
      else: 
        reward += (score-self.last_score)*10 - 1*penalty_weight

      self.last_score = score
    else: reward += - 1*penalty_weight

    num_same_act = self.count_consecutive_actions(action)
    reward += (num_same_act-1)*(-50)

    self.true_masteries = self.forget_update_masteries()
    # self.masteries = self.forget_update_masteries()
    if len(self.history)==MAX_NUM_ACTIONS or ACTIONS[int(action)]=='STOP' or check_mastered==True: done=True

    return self._get_obs(), reward, done, {}

  def reset(self):
    self.true_masteries = np.zeros(len(SKILL_INDS))
    for i,m in enumerate(self.true_masteries):
      self.true_masteries[i] = random.randint(0,SKILL_LEVELS[i])

    # self.masteries = np.zeros(len(SKILL_INDS))
    new_masteries = np.zeros(len(SKILL_INDS))
    for i,m in enumerate(new_masteries):
      self.true_masteries[i] = random.randint(0,SKILL_LEVELS[i])
    test = test_gen(SKILL_INDS,NUM_QUESTIONS_PER_TEST)
    self.masteries = self.test_update_masteries(test)

    score = self.get_test_score(test, self.true_masteries)
    self.last_score = score
    self.history = []
    return self._get_obs()

  def _get_obs(self):
    return [i*1.0 for i in self.masteries]

  def preview(self):
    print('Learning probability:',self.learn_prob)
    print('Forget rate:',self.forget_rate)
    print('Known answer rate:',self.answer_rate1)
    print('Unknown answer rate:',self.answer_rate2)
    print('Skill masteries:',self.masteries)

  def count_consecutive_actions(self, action):
    count = 1
    if len(self.history)==0: return count
    else:
      temp_acts = self.history
      temp_acts.reverse()
      for act in temp_acts:
        if act==action: count+=1
        else: break
      return count