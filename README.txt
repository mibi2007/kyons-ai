TODO list:
1. In environment.py/User:
    1a. Add API function to generate LESSONS list (actions list)
    1b. Update get_tests with API call to retrieve user's tests results (if any)
    1c. Update get_actions with API call to retrieve all actions previously selected for user 
2. In interact.py, update action_select_update with API call to pass agent's selected action to app's server

WORK FLOW
- When the app needs to choose an action, call interact.py;
- Create a new agent, then load its state by calling API to get test results, and load its history by calling API to get previous actions 
- The effect of last action can only be observed now with latest test result, so update model with latest test result 
- Let model choose next action and return result to app 