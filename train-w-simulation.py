from environment import SimStudent
from d4pgagent import D4PGAgent
from acme import wrappers
from acme import environment_loop
from acme.utils import loggers
import os
import random

def train(num_epochs=20, num_students=2):
    save_path = "./save/demo/"
    d4pg_agent = D4PGAgent()
    for stu in range(num_students):
        print('Training',stu+1,'/',num_students,'students...')
        intelligence = random.randint(50,100)
        env = SimStudent(intelligence)
        env = wrappers.GymWrapper(env)
        timestep = env.reset()
        logger = loggers.TerminalLogger(time_delta=10.)
        loop = environment_loop.EnvironmentLoop(env, d4pg_agent.d4pg_agent, logger=logger)
        loop.run(num_episodes=num_epochs)
        # checkpoint.save(save_path)
        d4pg_agent.save_model(save_path)
      
if __name__ == '__main__':
    train()