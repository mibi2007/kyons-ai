from environment import SimStudent
from d4pgagent import D4PGAgent

from acme import wrappers

def simulate(intelligence):
    d4pg_agent = D4PGAgent()

    env = SimStudent(intelligence)
    environment = wrappers.GymWrapper(env)
    timestep = environment.reset()

    while not timestep.last():
      action = d4pg_agent.d4pg_agent.select_action(timestep.observation)
      timestep = environment.step(action)
      # selected_lessons = [ACTIONS[int(i)] for i in environment.history]
      print('Skills:',timestep.observation, 'Score:',environment.last_score,  'Actions:', environment.history)
      
if __name__ == '__main__':
    print('Input simulated student intelligence:')
    intel = int(input())
    simulate(intel)